﻿using Plugin.FirebasePushNotification;
using Prism;
using Prism.Ioc;
using SamplePush.ViewModels;
using SamplePush.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SamplePush.Helpers;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SamplePush
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            CrossFirebasePushNotification.Current.Subscribe("General");

            CrossFirebasePushNotification.Current.OnTokenRefresh += (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine($"TOKEN REC: {p.Token}");
                ConstantsHelper.TokenNotifications = p.Token;
            };

            CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine("Opened");
                foreach (var data in p.Data)
                {
                    System.Diagnostics.Debug.WriteLine($"{data.Key} : {data.Value}");
                    if (data.Key == "funcionalidade") {
                        switch (data.Value) {
                            case "pageA":
                                NavigationService.NavigateAsync("/NavigationPage/MainPage/PageA");
                                break;
                            case "pabeB":
                                NavigationService.NavigateAsync("/NavigationPage/MainPage/PageB");
                                break;
                            default:
                                NavigationService.NavigateAsync("/NavigationPage/MainPage");
                                break;
                        }
                    }
                }
            };

            ConstantsHelper.TokenNotifications = CrossFirebasePushNotification.Current.Token;
            System.Diagnostics.Debug.WriteLine($"TOKEN: {CrossFirebasePushNotification.Current.Token}");

            await NavigationService.NavigateAsync("NavigationPage/MainPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<PageA, PageAViewModel>();
            containerRegistry.RegisterForNavigation<PageB, PageBViewModel>();
        }
    }
}
