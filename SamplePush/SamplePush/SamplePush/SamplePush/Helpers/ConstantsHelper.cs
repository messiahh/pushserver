﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SamplePush.Helpers
{
    public static class ConstantsHelper
    {
        /// <summary>
        /// Variavel especifica para uso no armazenamento de versões do aplicativo.
        /// </summary>
        public static string AppVersion;
        /// <summary>
        /// Variavel especifica para uso no armazenamento de versões do aplicativo.
        /// </summary>
        public static string AppBuildNumber;
        /// <summary>
        /// Variavel especifica para uso no processo de registro do token de notificações push. Não utilizar!!
        /// </summary>
        public static string TokenNotifications;
    }
}
